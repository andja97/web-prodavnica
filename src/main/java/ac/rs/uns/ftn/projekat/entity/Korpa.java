package ac.rs.uns.ftn.projekat.entity;

import javax.persistence.*;
import java.io.Serializable;
import java.util.*;

enum Status{Kupljeno, Dostava, Otkazano, Dostavljeno}

@Entity
public class Korpa implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name="Datum_porudzbine")
    private Date datumKupovine;
    @Column(name="Status")
    private Status status;
    @OneToOne()
    private Kupac kupac;
    @OneToOne()
    private Dostavljac dostavljac;
    @OneToMany(fetch =  FetchType.LAZY, cascade = CascadeType.ALL)
    private List<Artikal> sadrzaj;
    @ManyToOne(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    private Dostavljac dostavljaci;

    public List<Artikal> getSadrzaj() {
        return sadrzaj;
    }

    public void setSadrzaj(List<Artikal> sadrzaj) {
        this.sadrzaj = sadrzaj;
    }

    @OneToMany(mappedBy = "korpa", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    List<Artikal> listaKupljenihArtikala = new ArrayList<Artikal>();

    public Long getId() {
        return id;
    }

    public List<Artikal> getListaKupljenihArtikala() {
        return listaKupljenihArtikala;
    }

    public Date getDatumKupovine() {
        return datumKupovine;
    }

    public Status getStatus() {
        return status;
    }

    public Kupac getKupac() {
        return kupac;
    }

    public Dostavljac getDostavljac() {
        return dostavljac;
    }

    public Dostavljac getDostavljaci() {
        return dostavljaci;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public void setListaKupljenihArtikala(List<Artikal> listaKupljenihArtikala) {
        this.listaKupljenihArtikala = listaKupljenihArtikala;
    }

    public void setDatumKupovine(Date datumKupovine) {
        this.datumKupovine = datumKupovine;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public void setKupac(Kupac kupac) {
        this.kupac = kupac;
    }

    public void setDostavljac(Dostavljac dostavljac) {
        this.dostavljac = dostavljac;
    }

    public void setDostavljaci(Dostavljac dostavljaci) {
        this.dostavljaci = dostavljaci;
    }

    @Override
    public String toString() {
        return "KorpaRepository{" +
                "id=" + id +
                ", listaKupljenihArtikala=" + listaKupljenihArtikala +
                ", datumKupovine=" + datumKupovine +
                ", status=" + status +
                ", kupac=" + kupac +
                ", dostavljac=" + dostavljac +
                '}';
    }
}
