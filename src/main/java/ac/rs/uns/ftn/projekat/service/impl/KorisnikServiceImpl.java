package ac.rs.uns.ftn.projekat.service.impl;

import ac.rs.uns.ftn.projekat.entity.Artikal;
import ac.rs.uns.ftn.projekat.entity.Korisnik;
import ac.rs.uns.ftn.projekat.repository.KorisnikRep;
import ac.rs.uns.ftn.projekat.service.KorisnikService;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public  class KorisnikServiceImpl implements KorisnikService {

    @Autowired
    public KorisnikRep korisnikRep;

    @Override
    public Korisnik login(String korisnickoime, String lozinka) throws Exception {
        Korisnik newKorisnik = this.korisnikRep.findByKorisnickoimeAndLozinka(korisnickoime, lozinka);
        if (newKorisnik == null) {
            throw new Exception("Neispravni podaci!!");
        } else {
            return newKorisnik;
        }
    }

    @Override
    public Korisnik findOneByKorisnickoime(String korisnickoime) {
        return korisnikRep.findOneByKorisnickoime(korisnickoime);
    }

    @Override
    public Korisnik findOne(Long id) {
        Korisnik korisnik = this.korisnikRep.getOne(id);
        return korisnik;
    }

    @Override
    public Korisnik findOneForRegister(String ime) {
        Korisnik korisnik = this.korisnikRep.findByIme(ime);
        return korisnik;
    }

    @Override
    public Korisnik registracija(Korisnik korisnik) throws Exception {
        if (korisnikRep.findOneByKorisnickoime(korisnik.getKorisnickoime()) != null) {
            return null;
        } else {

            return korisnikRep.save(korisnik);
        }
    }

    @Override
    public List<Korisnik> findAll() {
        List<Korisnik> korisnici = this.korisnikRep.findAll();
        return korisnici;
    }


    @Override
    public void deleteAll(Long id) {
        Korisnik korisnik= korisnikRep.getOne(id);
         korisnikRep.delete(korisnik);
    }



}
