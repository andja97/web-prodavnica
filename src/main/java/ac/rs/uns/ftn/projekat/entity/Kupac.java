package ac.rs.uns.ftn.projekat.entity;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
public class Kupac extends Korisnik {
    @Column(name="Lista_kupljenih_proizvoda")
    @OneToMany(mappedBy = "kupac", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    List<Artikal> listaKupljenihArtikala = new ArrayList<Artikal>();

    @Column(name="Lista_omiljenih_proizvoda")
    @OneToMany(mappedBy = "kupac", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    List<Artikal> listaOmiljenihhArtikala = new ArrayList<Artikal>();

    @Column(name="Lista_proizvoda_u_korpi_proizvoda")
    @OneToMany(mappedBy = "kupac", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    List<Artikal> listaProizvodaUKorpi = new ArrayList<Artikal>();
}
