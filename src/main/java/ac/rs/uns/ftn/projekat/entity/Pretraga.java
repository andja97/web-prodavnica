package ac.rs.uns.ftn.projekat.entity;

public class Pretraga {

    private String pretraga;

    public String getPretraga() {
        return pretraga;
    }

    public void setPretraga(String pretraga) {
        this.pretraga = pretraga;
    }

    public Pretraga(){
        pretraga = new String();
    }
}
