package ac.rs.uns.ftn.projekat.repository;

import ac.rs.uns.ftn.projekat.entity.Korisnik;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import java.util.List;

@Repository
public interface KorisnikRep extends JpaRepository<Korisnik,Long> {
    Korisnik findByIme(String ime);
    Korisnik save(Korisnik korisnik);
    Korisnik findOneByKorisnickoime(String korisnickoime);
    Korisnik findByKorisnickoimeAndLozinka(String korisnickoime,String lozinka);
    List<Korisnik> findAll();

}

