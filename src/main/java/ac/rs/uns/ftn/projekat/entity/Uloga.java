package ac.rs.uns.ftn.projekat.entity;

public enum Uloga {
    KUPAC, ADMINISTRATOR, DOSTAVLJAC
};