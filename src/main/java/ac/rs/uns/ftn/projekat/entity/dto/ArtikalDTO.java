package ac.rs.uns.ftn.projekat.entity.dto;

public class ArtikalDTO {


    public ArtikalDTO(String nazvi, String opisArtikla, String kategorija, double cena) {
        this.nazvi = nazvi;
        this.opisArtikla = opisArtikla;
        this.kategorija = kategorija;
        this.cena = cena;
    }

    public String getNazvi() {
        return nazvi;
    }

    public void setNazvi(String nazvi) {
        this.nazvi = nazvi;
    }

    public String getOpisArtikla() {
        return opisArtikla;
    }

    public void setOpisArtikla(String opisArtikla) {
        this.opisArtikla = opisArtikla;
    }

    public String getKategorija() {
        return kategorija;
    }

    public void setKategorija(String kategorija) {
        this.kategorija = kategorija;
    }

    public double getCena() {
        return cena;
    }

    public void setCena(double cena) {
        this.cena = cena;
    }

    private String nazvi;
    private String opisArtikla;
    private String kategorija;
    private double cena;




}
