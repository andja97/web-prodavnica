package ac.rs.uns.ftn.projekat.controller;

import ac.rs.uns.ftn.projekat.entity.Artikal;
import ac.rs.uns.ftn.projekat.entity.Korisnik;
import ac.rs.uns.ftn.projekat.entity.Uloga;
import ac.rs.uns.ftn.projekat.entity.dto.KorisnikDTO;
import ac.rs.uns.ftn.projekat.service.ArtikalService;
import ac.rs.uns.ftn.projekat.service.KorisnikService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

import javax.validation.Valid;

@Controller
public class KorisnikController {

    @Autowired
    private KorisnikService korisnikService;


    @GetMapping("/home")
    public String welcome() {
        return "home.html";
    }

    @GetMapping("/registracija")
    public String registrovanje(Model model) {
        Korisnik korisnik = new Korisnik();
        model.addAttribute("korisnik", korisnik);
        return "registracija.html";
    }






    @PostMapping(value = "/registracija")
    public String addUser(
            @RequestParam String korisnickoime,
            @RequestParam String ime,
            @RequestParam String prezime,
            @RequestParam String lozinka,
            @RequestParam String email,
            @RequestParam Uloga  uloga,
            @RequestParam String telefon,
            @RequestParam String adresa
            ) {
        try {
            Korisnik korisnik = new Korisnik();
            korisnik.setKorisnickoime(korisnickoime);
            korisnik.setIme(ime);
            korisnik.setPrezime(prezime);
            korisnik.setUloga(uloga);
            korisnik.setLozinka(lozinka);
            korisnik.setTelefon(telefon);

            korisnik.setMail(email);
            korisnik.setAdresa(adresa);
            if(this.korisnikService.registracija(korisnik)==null){
                return "neuspesno.html";
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return "home.html";
    }

    public boolean chechkorisnickoime(String korisnickoime) {
        Korisnik korisnik = korisnikService.findOneForRegister(korisnickoime);
        if (korisnik == null) {
            return true;
        }
        return false;
    }

    @PostMapping("/sacuvatikorisnika")
    public String sacuvatikorisnika(@Valid @ModelAttribute Korisnik korisnik) throws Exception {
        if (chechkorisnickoime(korisnik.getKorisnickoime()) == true) {
            this.korisnikService.registracija(korisnik);
            return "redirect:/login";
        }
        return "redirect:/registracija";
    }

    @PostMapping("/login")
    public String login(@RequestParam String korisnickoime , @RequestParam String lozinka) {
        try{
        Korisnik korisnik = this.korisnikService.findOneByKorisnickoime(korisnickoime);

        if (korisnik.getLozinka().equals(lozinka)) {
            return "redirect:/home";
        } else {
            return "neuspesno.html";
        }

    } catch (Exception e) {
            return "neuspesno.html";
        }

    }
    @GetMapping("/login")
    public String getLogin(Model model){
        Korisnik korisnik = new Korisnik();
        model.addAttribute("korisnik", korisnik);
        return "login.html";
    }

    @GetMapping("/korisnici")
    public String korisnik(Model model) {
        List<Korisnik> korisnici = this.korisnikService.findAll();
        model.addAttribute("korisnici", korisnici);
        return "korisnici.html";
    }


    @GetMapping("/korisnici/{id}")
    public String getKorisnik(@PathVariable(name = "id") Long id, Model model) {
        Korisnik korisnik = this.korisnikService.findOne(id);
        model.addAttribute("korisnik", korisnik);
        return "korisnik.html";
    }
    @GetMapping("/logout")
    public String logout(Long id) {
     this.korisnikService.deleteAll(id);
        return "redirect:/login";
    }




}