package ac.rs.uns.ftn.projekat;
import ac.rs.uns.ftn.projekat.entity.Artikal;
import ac.rs.uns.ftn.projekat.repository.ArtikalRep;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.util.List;

@SpringBootApplication
public class ProjekatApplication {

	public static void main(String[] args) {
		SpringApplication.run(ProjekatApplication.class, args);
	}

}
