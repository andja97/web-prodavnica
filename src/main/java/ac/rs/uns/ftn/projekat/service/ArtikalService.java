package ac.rs.uns.ftn.projekat.service;

import ac.rs.uns.ftn.projekat.entity.*;

import java.util.List;

import org.springframework.stereotype.Service;

@Service
public interface ArtikalService {

    List<Artikal> findAll();
    List<Artikal> findAllByKategorija(String kategorija);


    List<Artikal> sortByNameRastuce();
    List<Artikal> sortByNameOpadajuce();
    List<Artikal> sortByCenaRastuca();
    List<Artikal> sortByCenaOpadajuca();


    Artikal dodajartikal(Artikal artikal)  throws  Exception;
    void deleteOne(String naziv);
}
