package ac.rs.uns.ftn.projekat.repository;

import com.sun.javafx.collections.SortableList;
import net.bytebuddy.TypeCache;
import org.springframework.data.jpa.repository.JpaRepository;
import ac.rs.uns.ftn.projekat.entity.Artikal;
import org.springframework.stereotype.Repository;

import javax.naming.ldap.SortKey;
import java.util.List;

@Repository
public interface ArtikalRep extends JpaRepository<Artikal,Long> {

    List<ArtikalRep> findAllById(Long id);

    Artikal getAllByKategorija(String kategorija);

    List<ArtikalRep> findByNaziv(String naziv);

    List<Artikal> findByKategorija(String kategorija);

    List<ArtikalRep> findByOpisArtikla(String opisArtikla);

    List<ArtikalRep> findByKolicina(int kolicina);

    Artikal findOneByKategorija(String kategorija);


    List<Artikal> findALLByOrderByNazivAsc();

    List<Artikal> findALLByOrderByNazivDesc();

    List<Artikal> findALLByOrderByCenaAsc();

    List<Artikal> findALLByOrderByCenaDesc();

    List<Artikal> findAllByNazivOrderByKategorija(String pretraga);

    Artikal save(Artikal artikal);

    Artikal findOneByNaziv(String naziv);
}
