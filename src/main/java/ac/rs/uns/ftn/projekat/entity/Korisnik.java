package ac.rs.uns.ftn.projekat.entity;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Inheritance(strategy=InheritanceType.JOINED)
public class Korisnik implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name="korisnickoime")
    private String korisnickoime;
    @Column(name="lozinka")
    private String lozinka;
    @Column(name="ime")
    private String ime;
    @Column(name="prezime")
    private String prezime;
    @Column(name="uloga")
    private Uloga uloga;
    @Column(name="telefon")
    private String telefon;
    @Column(name="email")
    private String email;
    @Column(name="adresa")
    private String adresa;

    @Override
    public String toString() {
        return "Kupac{" +
                "korisnickoIme='" + korisnickoime + '\'' +
                ", ime='" + ime + '\'' +
                ", prezime='" + prezime + '\'' +
                ", lozinka='" + lozinka + '\'' +
                ", uloga='" + uloga + '\'' +
                ", telefon=" + telefon +
                ", mail='" + email + '\'' +
                ", adresa='" + adresa + '\'' +
                '}';
    }
    @ManyToMany
    @JoinTable(name = "working",
            joinColumns = @JoinColumn(name = "employee_id", referencedColumnName = "id"),
            inverseJoinColumns = @JoinColumn(name = "project_id", referencedColumnName = "id"))

    public void setKorisnickoime(String korisnickoIme) {
        this.korisnickoime = korisnickoIme;
    }

    public void setIme(String ime) {
        this.ime = ime;
    }

    public void setPrezime(String prezime) {
        this.prezime = prezime;
    }

    public void setLozinka(String lozinka) {
        this.lozinka = lozinka;
    }

    public void setUloga(Uloga uloga) {
        this.uloga = uloga;
    }

    public void setTelefon(String telefon) {
        this.telefon = telefon;
    }

    public void setMail(String email) {
        this.email = email;
    }

    public void setAdresa(String adresa) {
        this.adresa = adresa;
    }

    public String getKorisnickoime() {
        return korisnickoime;
    }

    public String getIme() {
        return ime;
    }

    public String getPrezime() {
        return prezime;
    }

    public String getLozinka() {
        return lozinka;
    }

    public Uloga getUloga() {
        return uloga;
    }

    public String getTelefon() {
        return telefon;
    }

    public String getEmail() {
        return email;
    }

    public String getAdresa() {
        return adresa;
    }

    public Long getId(){ return id;}

    public void setId(Long id){this.id=id;}

}