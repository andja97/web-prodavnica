package ac.rs.uns.ftn.projekat.service.impl;

import ac.rs.uns.ftn.projekat.entity.*;
import ac.rs.uns.ftn.projekat.repository.*;
import ac.rs.uns.ftn.projekat.service.*;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.GetMapping;

@Service
public class ArtikalServiceImpl implements ArtikalService {

    @Autowired
    public ArtikalRep artikalRep;


    @Override
    public List<Artikal> findAll() {
        List<Artikal> artikali = this.artikalRep.findAll();
        return artikali;
    }



    @Override
    public List<Artikal> findAllByKategorija(String kategorija) {
        List <Artikal> artikali= this.artikalRep.findByKategorija(kategorija);
        return  artikali;
    }




    @Override
    public List<Artikal> sortByNameRastuce() {
        List<Artikal> artikali = this.artikalRep.findALLByOrderByNazivAsc();
        return artikali;
    }

    @Override
    public List<Artikal> sortByNameOpadajuce() {
        List<Artikal> artikali = this.artikalRep.findALLByOrderByNazivDesc();
        return artikali;
    }
    @Override
    public List<Artikal> sortByCenaRastuca() {
        List<Artikal> artikali = this.artikalRep.findALLByOrderByCenaAsc();
        return artikali;
    }

    @Override
    public List<Artikal> sortByCenaOpadajuca() {
        List<Artikal> artikali = this.artikalRep.findALLByOrderByCenaDesc();
        return artikali;
    }

    @Override
    public Artikal dodajartikal(Artikal artikal) throws Exception {

        return artikalRep.save(artikal);
    }
    @Override
    public void deleteOne(String naziv) {
        Artikal artikal= artikalRep.findOneByNaziv(naziv);
        artikalRep.delete(artikal);
    }


}
