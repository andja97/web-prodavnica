package ac.rs.uns.ftn.projekat.entity;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
public class Dostavljac extends Korisnik {

    @OneToMany(mappedBy = "dostavljaci", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    List<Korpa> listaPorudzbina = new ArrayList<Korpa>();

}
