package ac.rs.uns.ftn.projekat.controller;

import ac.rs.uns.ftn.projekat.entity.Artikal;
import ac.rs.uns.ftn.projekat.entity.Pretraga;
import ac.rs.uns.ftn.projekat.entity.dto.ArtikalDTO;
import ac.rs.uns.ftn.projekat.service.ArtikalService;
import ac.rs.uns.ftn.projekat.service.KorpaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.ArrayList;
import java.util.List;

@Controller
public class ArtikalController {
    @Autowired
    public ArtikalService artikalService;
    public KorpaService korpaService;

    @GetMapping("/kategorije/{kat}")
    public String pogledajKategorije(@PathVariable(name = "kat") String kategorija, Model model) {

        if(kategorija.toUpperCase().contains("DOMACI")) {
            List<Artikal> artikli = this.artikalService.findAllByKategorija(kategorija);

            model.addAttribute("artikli", artikli);
            return "domaci.html";
        }

        if(kategorija.toUpperCase().contains("ISTORIJSKI")) {
            List<Artikal> artikli = this.artikalService.findAllByKategorija(kategorija);

            model.addAttribute("artikli", artikli);

            return "istorijski.html";
        }
        if(kategorija.toUpperCase().contains("RUSKA")) {
            List<Artikal> artikli = this.artikalService.findAllByKategorija(kategorija);

            model.addAttribute("artikli", artikli);
            return "ruskaknjizevnost.html";
        }
        return "neuspesno.html";
    }
    @GetMapping("/sort/imerastuce")
    public String sortirajPoImenuRastuce( Model model) {


        List<Artikal> artikli = this.artikalService.sortByNameRastuce();

        model.addAttribute("artikli", artikli);
        model.addAttribute("ppretraga", new Pretraga());
        return "artikli.html";
    }

    @GetMapping("/sort/imeopadajuce")
    public String sortirajPoImenuOpadajuce( Model model) {


        List<Artikal> artikli = this.artikalService.sortByNameOpadajuce();

        model.addAttribute("artikli", artikli);
        model.addAttribute("ppretraga", new Pretraga());
        return "artikli.html";
    }

    @GetMapping("/sort/cenarastuca")
    public String sortirajPoCeniRastuca( Model model) {

        List<Artikal> artikli = this.artikalService.sortByCenaRastuca();

        model.addAttribute("artikli", artikli);
        model.addAttribute("ppretraga", new Pretraga());


        return "artikli.html";
    }

    @GetMapping("/sort/cenaopadajuca")
    public String sortirajPoCeniOpadajuca( Model model) {

        List<Artikal> artikli = this.artikalService.sortByCenaOpadajuca();

        model.addAttribute("artikli", artikli);
        model.addAttribute("ppretraga", new Pretraga());


        return "artikli.html";
    }

    @GetMapping("/artikli")
    public String pogledajArtikle(Model model) {

        List<Artikal> artikli = artikalService.findAll();



        model.addAttribute("artikli", artikli);
        model.addAttribute("ppretraga", new Pretraga());

        return "artikli.html";
    }

    @GetMapping("/popust")
    public String pogledajPopust(Model model) {

        List<Artikal> artikli = artikalService.findAll();

        List<Artikal> popust = new ArrayList<>();
        for(Artikal a : artikli)
        {
            if(a.getPopust())
                popust.add(a);
        }

        model.addAttribute("artikli", popust);
        model.addAttribute("ppretraga", new Pretraga());

        return "artikli.html";
    }
    @GetMapping("/omiljene")
    public String pogledajOmiljene(Model model) {

        List<Artikal> artikli = artikalService.findAll();

        List<Artikal> omiljen = new ArrayList<>();
        for(Artikal a : artikli)
        {
            if(a.getOmiljen())
                omiljen.add(a);
        }

        model.addAttribute("artikli", omiljen);
        model.addAttribute("ppretraga", new Pretraga());

        return "artikli.html";
    }
    @PostMapping("/pretrazi")
    public String pretrazi(@ModelAttribute Pretraga pretraga2, Model model) {

        String pretraga = pretraga2.getPretraga();

        List<Artikal> artikli = artikalService.findAll();
        List<Artikal> art = new ArrayList<>();
        for(Artikal a : artikli)
        {
            if(a.getNaziv().toUpperCase().contains(pretraga.toUpperCase()) || Double.toString(a.getCena()).toUpperCase().contains(pretraga.toUpperCase()) || a.getNaziv().toUpperCase().contains(pretraga.toUpperCase()) || a.getNaziv().toUpperCase().contains(pretraga.toUpperCase()) || a.getOpisArtikla().toUpperCase().contains(pretraga.toUpperCase()) || a.getKategorija().toUpperCase().contains(pretraga.toUpperCase())) {
                art.add(a);
            }
        }

        model.addAttribute("artikli", art);
        model.addAttribute("ppretraga", new Pretraga());
        return "artikli.html";

    }

    List<Artikal> arr;

    @GetMapping("/korpa")
    public String getKorpa(Model model) {



        model.addAttribute("artikli", arr);
        model.addAttribute("ppretraga", new Pretraga());

        return "korpa.html";
    }

    @PostMapping("/artikl")
    public String dodajUKorpuu(@Valid @ModelAttribute Artikal artikal, @ModelAttribute Long korpa_id, Model model) {


        return "redirect:/artikli";

    }


    //    @PostMapping("/pretrazii")
//        public String pretrazii(@ModelAttribute Pretraga pretraga1,@ModelAttribute Pretraga pretraga2  Model model) {
//
////        String pretraga1 = pretraga1.getPretraga();
//
//        List<Artikal> artikli = artikalService.findAll();
//        List<Artikal> art = new ArrayList<>();
//        for(Artikal a : artikli)
//        {
//            if(a.getNaziv().toUpperCase().contains(pretraga1.toUpperCase())  a.getNaziv().toUpperCase().contains(pretraga.toUpperCase()) || a.getOpisArtikla().toUpperCase().contains(pretraga.toUpperCase()) || a.getKategorija().toUpperCase().contains(pretraga.toUpperCase())) {
//                art.add(a);
//            }
//        }
//        model.addAttribute("artikli", art);
//        model.addAttribute("ppretraga", new SkupPretraga());
//        return "artikli.html";
//
//    }
    @GetMapping("/dodajartikal")
    public String dodajartikal(Model model) {
        Artikal artikal = new Artikal();
        model.addAttribute("artikal", artikal);
        model.addAttribute("ppretraga", new Pretraga());
        return "dodajartikal.html";
    }

    @PostMapping(value = "/dodajartikal")
    public String addArtikal(
            @RequestParam String naziv,
            @RequestParam String opisArtikla,
            @RequestParam Double cena,
            @RequestParam int kolicina,
            @RequestParam String kategorija,
            @RequestParam Boolean popust,
            @RequestParam Boolean omiljen
    ) {
        try {
           Artikal artikal = new Artikal();
            artikal.setNaziv(naziv);
            artikal.setOpisArtikla(opisArtikla);
            artikal.setCena(cena);
            artikal.setKolicina(kolicina);
            artikal.setKategorija(kategorija);
            artikal.setPopust(popust);

            artikal.setOmiljen(omiljen);
           this.artikalService.dodajartikal(artikal);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return "home.html";
    }

    @PostMapping("/sacuvatiartikal")
    public String sacuvatiartikal(@Valid @ModelAttribute Artikal artikal) throws Exception {
            this.artikalService.dodajartikal(artikal);
            return "redirect:/home";
    }

    @GetMapping("/obrisi")
    public String logout(String naziv) {
        this.artikalService.deleteOne(naziv);
        return "redirect:/home";
    }
}
