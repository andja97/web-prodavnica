package ac.rs.uns.ftn.projekat.service;

import ac.rs.uns.ftn.projekat.entity.Artikal;
import ac.rs.uns.ftn.projekat.entity.Korisnik;

import java.util.List;

import org.springframework.stereotype.Service;

@Service
public interface KorisnikService {

    Korisnik findOneByKorisnickoime(String korisnickoime) ;
    Korisnik findOneForRegister(String ime);
    Korisnik findOne(Long id);
    Korisnik login(String korisnickoime,String lozinka) throws Exception;
    Korisnik registracija(Korisnik korisnik)  throws  Exception;

	List<Korisnik> findAll();


    void deleteAll(Long id);

}
