// Name and Password from the register-form
var ime = document.getElementById('ime');
var lozinka = document.getElementById('lozinka');

// storing input from register-form
function store() {
    localStorage.setItem('ime', ime.value);
    localStorage.setItem('lozinka', lozinka.value);
}

// check if stored data from register-form is equal to entered data in the   login-form
function check() {

    // stored data from the register-form
    var storedIme = localStorage.getItem('ime');
    var storedLozinka = localStorage.getItem('lozinka');

    // entered data from the login-form
    var korisnickoime = document.getElementById('korisnickoime');
    var userLozinka = document.getElementById('userLozinka');

    // check if stored data from register-form is equal to data from login form
    if(korisnickoime.value !== storedIme || userLozinka.value !== storedLozinka) {
        alert('ERROR');
    }else {
        alert('You are loged in.');
    }
}
